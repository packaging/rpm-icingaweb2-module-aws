# AWS Module for Icinga Web 2 | (c) 2018-2019 Icinga Development Team <info@icinga.com> | GPLv2+

%global revision 1
%global module_name aws

%global icingaweb_min_version 2.6.0

Name:           icingaweb2-module-%{module_name}
Version:        0.6.0
Release:        %{revision}%{?dist}
Summary:        AWS - Icinga Web 2 module
Group:          Applications/System
License:        GPLv2+
URL:            https://icinga.com
Source0:        https://github.com/Icinga/icingaweb2-module-%{module_name}/archive/v%{version}.tar.gz
#/icingaweb2-module-%{module_name}-%{version}.tar.gz
BuildArch:      noarch

%global basedir %{_datadir}/icingaweb2/modules/%{module_name}

Requires:       icingaweb2 >= %{icingaweb_min_version}
Requires:       php-Icinga >= %{icingaweb_min_version}

Requires:       icingaweb2-module-director >= 1.6

%description
This is a simple Amazon Web Services (AWS) module for Icinga Web 2. Currently
it is nothing but an Import Source provider for Icinga Director.

It allows you to configure an Director automation that new Auto Scaling Groups
would be deployed immediately as new (virtual) hosts to your Icinga monitoring
system.

%prep
%setup -q
#-n icingaweb2-module-%{module_name}-%{version}

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{basedir}

cp -r * %{buildroot}%{basedir}

%clean
rm -rf %{buildroot}

%preun
set -e

# Only for removal
if [ $1 == 0 ]; then
    echo "Disabling icingaweb2 module '%{module_name}'"
    rm -f /etc/icingaweb2/enabledModules/%{module_name}
fi

exit 0

%files
%doc README.md
#LICENSE

%defattr(-,root,root)
%{basedir}

%changelog
* Fri Aug 30 2019 Markus Frosch <markus.frosch@icinga.com> - 0.6.0-1
- Initial package version
